#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Power.h"
#include <vector>
#include "text.h"
class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;
	Enemy* enemy;

	float spawnTime;
	float currentSpawnTimer;
	std::vector<Enemy*> spawnEnemies;
	std::vector<Power*> spawnP;
	float pSpawn;
	float currentPSpawn;

	void doSpawnLogic();
	void doCollisionLogic();
	void spawn();
	void spawnPowerup();
	void despawnEnemy(Enemy* enemy);
	void despawnPowerup(Power* power);

	int points;
};

