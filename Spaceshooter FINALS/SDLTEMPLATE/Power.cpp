#include "Power.h"
Power::Power(float positionX, float positionY)
{
	this->x = positionX;
	this->y = positionY;
}

void Power::start()
{
	width = 0;
	height = 0;
	
	texture = loadTexture("gfx/skull.png");

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Power::update()
{
}

void Power::draw()
{
	blit(texture, x, y);
}

int Power::getPositionX()
{
	return x;
}

int Power::getPositionY()
{
	return y;
}

int Power::getWidth()
{
	return width;
}

int Power::getHeight()
{
	return height;
}

void Power::setPosition(int xPos, int yPos)
{
	x = xPos;
	y = yPos;
}
