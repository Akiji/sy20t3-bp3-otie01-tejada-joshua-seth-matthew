#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 300;
	pSpawn = 600;
	currentPSpawn = 600;


	for (int i = 0; i < 4; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "DEADEDED!!!");
	}

}

void GameScene::update()
{
	Scene::update();
	doSpawnLogic();
	doCollisionLogic();
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawn();
		}
		currentSpawnTimer = spawnTime;

	}

	if (pSpawn > 0)
		currentPSpawn--;

	if (currentPSpawn <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawnPowerup();

		}
		currentPSpawn = pSpawn;
	}
}

void GameScene::doCollisionLogic()
{
	for (int i = 0; i < objects.size(); i++)
	{
		Bullet* bullet = dynamic_cast<Bullet*> (objects[i]);

		if (bullet != NULL)
		{
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight());

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnEnemies[i];
					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight());

					if (collision == 1)
					{
						points++;
						despawnEnemy(currentEnemy);
						break;
					}
				}
			}
		}
	}

	for (int i = 0; i < objects.size(); i++)
	{
		Power* pow = dynamic_cast<Power*> (objects[i]);

		if (pow != NULL)
		{
			int collision = checkCollision(
				player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
				pow->getPositionX(), pow->getPositionY(), pow->getWidth(), pow->getHeight());

			if (collision == 1)
			{
				int index = -1;
				for (int i = 0; i < spawnP.size(); i++)
				{
					if (pow == spawnP[i])
					{
						index = i;
						break;
					}
				}
				// If any match is found
				if (index != -1)
				{
					spawnP.erase(spawnP.begin() + index);
					delete pow;
					if (player->getPow() < 1)player->setPow(player->getPow() + 1);
				}
				break;
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition((rand() % 180 + 360), 10);
	spawnEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnEnemies.size(); i++)
	{
		if (enemy == spawnEnemies[i])
		{
			index = i;
			break;
		}
	}
	
	if (index != -1)
	{
		spawnEnemies.erase(spawnEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::spawnPowerup()
{
	Power* power = new Power(360, 500);
	spawnP.push_back(power);
	this->addGameObject(power);
}

void GameScene::despawnPowerup(Power* power)
{
	int index = -1;
	for (int i = 0; i < spawnP.size(); i++)
	{
		if (power == spawnP[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spawnP.erase(spawnP.begin() + index);
		delete enemy;
	}
}