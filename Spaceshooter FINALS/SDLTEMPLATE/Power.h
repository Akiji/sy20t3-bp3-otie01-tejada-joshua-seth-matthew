#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class Power :
	public GameObject
{
public:
	Power(float positionX, float positionY);
	void start();
	void update();
	void draw();
	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	void setPosition(int xPos, int yPos);

private:
	SDL_Texture* texture;
	int x;
	int y;
	int width;
	int height;
};

